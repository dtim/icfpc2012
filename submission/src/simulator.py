# coding=utf-8

from cell import *
import mine

class Simulator:
    def __init__(self, mine):
        self.mine = mine
        self.score = 0
        self.collected_gold = 0
        self.terminated = False
    
    def step(self, command):
        if self.terminated:
            return

        self.update_robot(command)
        self.update()
    
    def update_robot(self, command):
        if command == 'A':
            # Abort simulation, costs nothing
            self.abort()
        elif command in ['L', 'R', 'U', 'D']:
            if self.robot_can_move(command):
                self.score = self.score - 1                
                self.move_robot(command)
            else:
                # cannot move, wait instead
                command = 'W'
            
        if command == 'W':
            # Wait: do nothing, costs 1 point
            self.score = self.score - 1
            
    def move_robot(self, direction):
        old_position = self.mine.robot_position
        new_position = self.move_position(old_position, direction)
        moved_into = self.mine[new_position]
        self.eat(moved_into)
        if moved_into.type() == Cell.ROCK:
            old_rock_pos = new_position
            new_rock_pos = self.move_position(old_rock_pos, direction)
            self.mine[new_rock_pos] = self.mine[old_rock_pos]
        self.mine[new_position] = self.mine[old_position]
        self.mine[old_position] = mine.EMPTY_CELL
        self.mine.robot_position = new_position
    
    def eat(self, food):
        if food.type() == Cell.GOLD:
            # very tasty
            self.score = self.score + 25
            self.collected_gold = self.collected_gold + 1
        if food.type() == Cell.LIFT:
            self.win()
    
    def update(self):
        original = self.mine
        updated = self.mine.clone()
        for x in range(1,  original.width + 1):
            for y in range(1, original.height + 1):
                if original[x, y].type() == Cell.ROCK and original[x, y - 1].type() == Cell.EMPTY:
                    updated[x, y - 1] = original[x, y]
                    updated[x, y] = mine.EMPTY_CELL
                    if updated[x, y - 2].type() == Cell.ROBOT:
                        self.death()
                    continue
                if original[x, y].type() == Cell.ROCK and original[x, y - 1].type() == Cell.ROCK \
                    and original[x - 1, y - 1].type() == Cell.EMPTY and original[x - 1, y].type() == Cell.EMPTY:
                    updated[x - 1, y - 1] = original[x, y]
                    updated[x, y] = mine.EMPTY_CELL
                    if updated[x - 1, y - 2].type() == Cell.ROBOT:
                        self.death()
                    continue
                if original[x, y].type() == Cell.ROCK and original[x, y - 1].type() == Cell.ROCK \
                    and original[x + 1, y - 1].type() == Cell.EMPTY and original[x + 1, y].type() == Cell.EMPTY:
                    updated[x + 1, y - 1] = original[x, y]
                    updated[x, y] = mine.EMPTY_CELL
                    if updated[x + 1, y - 2].type() == Cell.ROBOT:
                        self.death()
                    continue
                if original[x, y].type() == Cell.ROCK and original[x, y - 1].type() == Cell.GOLD \
                    and original[x + 1, y - 1].type() == Cell.EMPTY and original[x + 1, y].type() == Cell.EMPTY:
                    updated[x + 1, y - 1] = original[x, y]
                    updated[x, y] = mine.EMPTY_CELL
                    if updated[x + 1, y - 2].type() == Cell.ROBOT:
                        self.death()
                    continue

        original.update(updated)
        if self.collected_gold == self.mine.gold:
            self.openExit()

    def openExit(self):
        for x in range(1, self.mine.width + 1):
            for y in range(1, self.mine.height + 1):
                if self.mine[x, y].type() == Cell.LIFT:
                    print 'openLift'
                    self.mine[x, y] = LiftCell(True)

    def death(self):
        self.terminated = True
        self.score = 0
        print 'LOOSER!'

    def win(self):
        self.terminated = True
        self.score += self.collected_gold * 50
        print 'WINNER!'
        print 'Scores: ', self.score

    def abort(self):
        self.terminated = True
        print 'ABORT!'
        print 'Scores: ', self.score        
    
    def robot_can_move(self, direction):
        new_position = self.move_position(self.mine.robot_position, direction)
        new_cell = self.mine[new_position]
        if new_cell.type() == Cell.EMPTY or new_cell.type() == Cell.EARTH or new_cell.type() == Cell.GOLD:
            # It's always possible to move to empty space,
            # eat some earth or take lambda
            return True
        if new_cell.type() == Cell.ROCK:
            # Robot can move a rock if there is some empty space behind
            # It is only possible if robot moves left or right
            if direction == 'U' or direction == 'D':
                return False
            behind_rock = self.move_position(new_position, direction)
            return self.mine[behind_rock].type() == Cell.EMPTY
        # Otherwise robot can't move
        if new_cell.type() == Cell.LIFT and new_cell.is_open:
            return True
        return False

    def move_position(self, position, direction):
        if direction == 'L':
            return (position[0] - 1, position[1])
        if direction == 'R':
            return (position[0] + 1, position[1])
        if direction == 'U':
            return (position[0], position[1] + 1)
        if direction == 'D':
            return (position[0], position[1] - 1)
        return position
