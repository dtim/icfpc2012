# coding=utf-8

# Cell types
class Cell:
    EMPTY    = 0
    ROBOT    = 1
    WALL     = 2
    ROCK     = 3
    EARTH    = 4
    GOLD     = 5
    LIFT     = 6
    TELEPORT = 7
    TARGET   = 8
    
# Empty cell
class EmptyCell:
    def type(self):
        return Cell.EMPTY
    
    def show(self):
        return ' '
        
    def bitmap(self):
        return "empty"

# Robot
class RobotCell:
    def type(self):
        return Cell.ROBOT
    
    def show(self):
        return 'R'
        
    def bitmap(self):
        return "robot"

# Wall
class WallCell:
    def type(self):
        return Cell.WALL
    
    def show(self):
        return '#'
        
    def bitmap(self):
        return "wall"

# Rock
class RockCell:
    def type(self):
        return Cell.ROCK
    
    def show(self):
        return '*'
        
    def bitmap(self):
        return "rock"

# Earth
class EarthCell:
    def type(self):
        return Cell.EARTH
    
    def show(self):
        return '.'
        
    def bitmap(self):
        return "earth"

# Gold
class GoldCell:
    def type(self):
        return Cell.GOLD
    
    def show(self):
        return '\\'
        
    def bitmap(self):
        return "lambda"

# Lift
class LiftCell:
    def __init__(self, is_open = False):
        self.is_open = is_open
            
    def type(self):
        return Cell.LIFT
    
    def show(self):
        if self.is_open:
            return 'O'
        return 'L'
    
    def bitmap(self):
        if self.is_open:
            return "openlift"
        return "closedlift"

# Teleport
class TeleportCell:
    def __init__(self, label):
        self.label = label
        self.target = None
        self.position = None
    
    def type(self):
        return Cell.TELEPORT
    
    def show(self):
        return self.label
    
    def bitmap(self):
        return "teleport"
    
    def set_target(self, target):
        self.target = target
    
# Teleport target
class TargetCell:
    def __init__(self, label):
        self.label = label
        self.position = None
        self.sources = []
    
    def type(self):
        return Cell.TARGET
    
    def show(self):
        return self.label
    
    def bitmap(self):
        return "target"
    
    def add_source(self, source):
        self.sources.append(source)
