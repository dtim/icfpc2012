#!/usr/bin/python
# coding: utf-8

import sys
import random
from robot import *
from mine import *
from cell import *
from simulator import *

invert = { "L":"R", "U":"D", "R":"L", "D":"U" }

class PlannerRobot(ThinkingRobot):
    def __init__(self, mine):
        ThinkingRobot.__init__(self, mine)
        self.original_map = mine.clone()

    def find_path(self, position, grail):
        wave = {position : "W"}
        border = [position]
        
        grailPosition = None
        searchMore = True
        while searchMore:
            new_border = []
            for b in border:
                if self.mine[b].type() == grail:
                    # grail found, time to stop planning and take it
                    grailPosition = b
                    searchMore = False
                    break
                for d in "LURD":
                    if self.simulator.robot_can_move_from(b, d):
                        new_position = self.simulator.move_position(b, d)
                        if new_position not in wave:
                            new_border.append(new_position)
                            wave[new_position] = invert[d]
            if not new_border:
                # We have seen all, time to leave
                searchMore = False
            # we have so many cells to visit
            border = new_border

        # have we found some gold?
        if grailPosition != None:
            d = wave[grailPosition]
            p = grailPosition
            moves = []
            
            while d != "W":
                moves.append(invert[d])
                p = self.simulator.move_position(p, d)
                d = wave[p]
            moves.reverse()
            return moves
        return []
    
    def go(self, path):
        for d in path:
            self.simulator.move_robot(d)
            if self.simulator.terminated:
                return False
        return True
    
    def proceed(self):
        my_position = self.mine.robot_position
        grail = Cell.GOLD
        if self.simulator.all_collected():
            grail = Cell.LIFT
        path = self.find_path(my_position, grail)
        if path:
            alive = self.go(path)
            if alive:
                return ''.join(path)
        return "A"

mine = Mine()
mine.parse(sys.stdin.readlines())
robot = PlannerRobot(mine)
robot.run()
