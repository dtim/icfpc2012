# coding: utf-8
import sys
import random
import subprocess
from simulator import *

class Robot:
    def __init__(self, mine):
        self.mine = mine

    def proceed(self):
        return "A"

class CrazyRobot(Robot):
    def proceed(self):
        return random.choice(("L", "R", "U", "D"))

class InteractiveRobot(Robot):
    def __init__(self, mine):
        Robot.__init__(self, mine)
        self.queue = []
    
    def proceed(self):
        cmd = self.queue.pop(0)
        sys.stdout.write(cmd)
        return cmd

    def command_from_user(self, command):
        self.queue.append(command)

# Proxy robot
# Runs an external program and reads its output

class ProxyRobot(Robot):
    def __init__(self, mine, program):
        Robot.__init__(self, mine)
        self.program = program
        self.proc = subprocess.Popen([program],
                                    stdin=subprocess.PIPE, \
                                    stdout=subprocess.PIPE)
        
        minedesc = mine.dump()
        istreams = self.proc.communicate(minedesc)
        self.queue = list(istreams[0])
    
    def proceed(self):
        if not self.queue:
            return "A"
        return self.queue.pop(0)

# Base thinking (simulating) robot
class ThinkingRobot(Robot):
    def __init__(self, mine):
        Robot.__init__(self, mine)
        self.simulator = Simulator(mine)
        self.command_count = 0
        self.command_limit = mine.height * mine.width
    
    def run(self):
        canWork = True
        while canWork:
            commands = self.proceed()
            self.submit_command(commands)
            if 'A' in commands or self.too_many_commands():
                canWork = False
        sys.stdout.write("\n")
        sys.stdout.flush()
          
    def too_many_commands(self):
        return self.command_count > self.command_limit
    
    def submit_command(self, command):
        self.command_count += len(command)
        sys.stdout.write(command)
