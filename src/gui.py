#!/usr/bin/python
# coding=utf-8

import sys
import getopt
import pygame
from mine import Mine
from cell import *
from robot import *
from simulator import Simulator

# ...
from astar import AStar
from sound import Sound

class Visualizer:
    BLACK = (0, 0, 0)
    track_bitmap = pygame.image.load("bitmap/track16.bmp")
    
    def __init__(self, mine):
        self.mine = mine
        
        self.blocksize = 16
        self.width = mine.width * self.blocksize
        self.height = mine.height * self.blocksize
        self.display = pygame.display.set_mode((self.width, self.height))
        self.clock = pygame.time.Clock()
        self.bitmaps = {}

    def load_bitmap(self, bmp):
        bmpfile = "bitmap/%(name)s%(size)d.bmp" % \
            { "name" : bmp, "size" : self.blocksize }
        self.bitmaps[bmp] = pygame.image.load(bmpfile)

    def draw(self, route_coords):
        r = 0
        c = 0
        self.display.fill(Visualizer.BLACK)
        for row in self.mine.rows:
            for cell in row:
                if cell.type() != Cell.EMPTY:
                    bmp = cell.bitmap()
                    if bmp not in self.bitmaps:
                        self.load_bitmap(bmp)
                    self.display.blit(self.bitmaps[bmp], (c,r))
                c = c + self.blocksize
            r = r + self.blocksize
            c = 0
    
        for i in route_coords:
            self.display.blit(self.track_bitmap, \
                (i[1] * self.blocksize, i[0] * self.blocksize))

        pygame.display.flip()

if __name__ == '__main__':
    optlist, args = getopt.getopt(sys.argv[1:], "e:c")
    mine_input = sys.stdin
    if args:
        mine_input = open(args[0])

    sound = Sound()

    mine = Mine()
    mine.parse(mine_input.readlines())
    sim = Simulator(mine)


#    astar = AStar(mine)
#    astar.prepare_map()
 
    #print 'Map size (X,Y): ', n, m
    #print 'Start: ', astar.xA, astar.yA
    #print 'Finish: ', astar.xB, astar.yB
    #t = time.time()
    #    print 'Time to generate the route (seconds): ', time.time() - t
#    astar.pathFind()
#    print 'Route:'
#    route_coords = astar.mark_route()
#    print astar.print_route()
#    print astar.print_map()

    
    robot = None
    if optlist:
        (option, value) = optlist[0]
        if option == '-e':
            print("ProxyRobot %s" % value)
            robot = ProxyRobot(mine, value)
        elif option == '-c':
            print("CrazyRobot")
            robot = CrazyRobot(mine)
    if not robot:
        print("InteractiveRobot")
        robot = InteractiveRobot(mine)
 
    pygame.init()
    screen = Visualizer(mine)

    done = False
    pygame.key.set_repeat(100, 100)
    while not (done):
        screen.draw(())
        pygame.display.update()
        if isinstance(robot, InteractiveRobot) and len(robot.queue) == 0:
            pass
        else:
            sim.step(robot.proceed())
        
        for event in pygame.event.get():
            if event.type == pygame.QUIT or \
                (event.type == pygame.KEYUP and event.key == pygame.K_ESCAPE):
                done = True
                break
            if isinstance(robot, InteractiveRobot):
                if event.type == pygame.KEYDOWN and event.key == pygame.K_UP:
                    robot.command_from_user('U')
		    sound.moveSound()
                if event.type == pygame.KEYDOWN and event.key == pygame.K_RIGHT:
                    robot.command_from_user('R')
		    sound.moveSound()
                if event.type == pygame.KEYDOWN and event.key == pygame.K_DOWN:
                    robot.command_from_user('D')
		    sound.moveSound()
                if event.type == pygame.KEYDOWN and event.key == pygame.K_LEFT:
                    robot.command_from_user('L')
		    sound.moveSound()
                if event.type == pygame.KEYDOWN and event.key == pygame.K_RETURN:
                    robot.command_from_user('A')
                if event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
                    robot.command_from_user('W')
        pygame.time.delay(28)
