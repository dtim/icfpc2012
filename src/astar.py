# A* Shortest Path Algorithm
# http://en.wikipedia.org/wiki/A*
# FB - 201012256
from heapq import heappush, heappop # for priority queue
import math
import time
import random

from mine import Mine

class node:
    xPos = 0 # x position
    yPos = 0 # y position
    distance = 0 # total distance already travelled to reach the node
    priority = 0 # priority = distance + remaining distance estimate
    def __init__(self, xPos, yPos, distance, priority):
        self.xPos = xPos
        self.yPos = yPos
        self.distance = distance
        self.priority = priority
    def __lt__(self, other): # comparison method for priority queue
        return self.priority < other.priority
    def updatePriority(self, xDest, yDest):
        self.priority = self.distance + self.estimate(xDest, yDest) * 10 # A*
    # give higher priority to going straight instead of diagonally
    def nextMove(self, dirs, d): # d: direction to move
        if dirs == 8 and d % 2 != 0:
            self.distance += 14
        else:
            self.distance += 10
    # Estimation function for the remaining distance to the goal.
    def estimate(self, xDest, yDest):
        xd = xDest - self.xPos
        yd = yDest - self.yPos
        # Euclidian Distance
        #d = math.sqrt(xd * xd + yd * yd)
        # Manhattan distance
        d = abs(xd) + abs(yd)
        # Chebyshev distance
        # d = max(abs(xd), abs(yd))
        return(d)

# A-star algorithm.
# The path returned will be a string of digits of directions.
class AStar:
    dirs = 4 # number of possible directions to move on the map
    the_map = []
    dx = [1, 0, -1, 0]
    dy = [0, 1, 0, -1]
    xA = 0
    yA = 0
    xB = 0
    yB = 0
    n = 0
    m = 0
    route = ''
    extended_path = ''
    route_coords = []
    
    def __init__(self, mine):
        self.n = mine.width
        self.m = mine.height
        self.mine = mine

    def pathFind(self):
        closed_nodes_map = [] # map of closed (tried-out) nodes
        open_nodes_map = [] # map of open (not-yet-tried) nodes
        dir_map = [] # map of dirs
        row = [0] * self.n
        for i in range(self.m): # create 2d arrays
            closed_nodes_map.append(list(row))
            open_nodes_map.append(list(row))
            dir_map.append(list(row))

        pq = [[], []] # priority queues of open (not-yet-tried) nodes
        pqi = 0 # priority queue index
        # create the start node and push into list of open nodes
        n0 = node(self.xA, self.yA, 0, 0)
        n0.updatePriority(self.xB, self.yB)
        heappush(pq[pqi], n0)
        open_nodes_map[self.yA][self.xA] = n0.priority # mark it on the open nodes map

        # A* search
        while len(pq[pqi]) > 0:
            # get the current node w/ the highest priority
            # from the list of open nodes
            n1 = pq[pqi][0] # top node
            n0 = node(n1.xPos, n1.yPos, n1.distance, n1.priority)
            x = n0.xPos
            y = n0.yPos
            heappop(pq[pqi]) # remove the node from the open list
            open_nodes_map[y][x] = 0
            closed_nodes_map[y][x] = 1 # mark it on the closed nodes map
    
            # quit searching when the goal is reached
            # if n0.estimate(xB, yB) == 0:
            if x == self.xB and y == self.yB:
                # generate the path from finish to start
                # by following the dirs
                path = ''
                while not (x == self.xA and y == self.yA):
                    j = dir_map[y][x]
                    c = str((j + self.dirs / 2) % self.dirs)
                    path = c + path
                    x += self.dx[j]
                    y += self.dy[j]
                self.route = path
                return True

            # generate moves (child nodes) in all possible dirs
            for i in range(self.dirs):
                xdx = x + self.dx[i]
                ydy = y + self.dy[i]
                if not (xdx < 0 or xdx > self.n-1 or ydy < 0 or ydy > self.m - 1
                        or self.the_map[ydy][xdx] == 1 or closed_nodes_map[ydy][xdx] == 1):
                    # generate a child node
                    m0 = node(xdx, ydy, n0.distance, n0.priority)
                    m0.nextMove(self.dirs, i)
                    m0.updatePriority(self.xB, self.yB)
                    # if it is not in the open list then add into that
                    if open_nodes_map[ydy][xdx] == 0:
                        open_nodes_map[ydy][xdx] = m0.priority
                        heappush(pq[pqi], m0)
                        # mark its parent node direction
                        dir_map[ydy][xdx] = (i + self.dirs / 2) % self.dirs
                    elif open_nodes_map[ydy][xdx] > m0.priority:
                        # update the priority
                        open_nodes_map[ydy][xdx] = m0.priority
                        # update the parent direction
                        dir_map[ydy][xdx] = (i + self.dirs / 2) % self.dirs
                        # replace the node
                        # by emptying one pq to the other one
                        # except the node to be replaced will be ignored
                        # and the new node will be pushed in instead
                        while not (pq[pqi][0].xPos == xdx and pq[pqi][0].yPos == ydy):
                            heappush(pq[1 - pqi], pq[pqi][0])
                            heappop(pq[pqi])
                        heappop(pq[pqi]) # remove the target node
                        # empty the larger size priority queue to the smaller one
                        if len(pq[pqi]) > len(pq[1 - pqi]):
                            pqi = 1 - pqi
                        while len(pq[pqi]) > 0:
                            heappush(pq[1-pqi], pq[pqi][0])
                            heappop(pq[pqi])       
                        pqi = 1 - pqi
                        heappush(pq[pqi], m0) # add the better node instead
        self.route = ''
        return False # if no route found

    def print_route(self):
        str = ''
        for i in range(len(self.route)):
            if self.route[i] == "0":
                str += "R"
            elif self.route[i] == "1":
                str += "D"
            elif self.route[i] == "2":
                str += "L"
            elif self.route[i] == "3":
                str += "U"
	str += "A"
        return str

    def search(self, y, x):
                self.the_map[y][x] = 3
		if y+1 < self.m and ( (y+1,x) not in self.route_coords ) and self.mine.rows[y+1][x].show() == "\\":
			self.extended_path += "1"
			self.route_coords.append( (y+1,x) )
			self.search (y+1,x)
		if y-1 >= 0 and ( (y-1,x) not in self.route_coords ) and self.mine.rows[y-1][x].show() == "\\":
			self.extended_path += "3"
			self.route_coords.append( (y-1,x) )
			self.search (y-1,x)
		if x+1 < self.n and ( (y,x+1) not in self.route_coords ) and self.mine.rows[y][x+1].show() == "\\":
			self.extended_path += "0"
			self.route_coords.append( (y,x+1) )
			self.search (y,x+1)
		if x-1 >= 0 and ( (y,x-1) not in self.route_coords ) and self.mine.rows[y][x-1].show() == "\\":
			self.extended_path += "2"
			self.route_coords.append( (y,x-1) )
			self.search (y,x-1)
		return True

    def mark_route(self):
        # mark the route on the map
        if len(self.route) > 0:
            x = self.xA
            y = self.yA
            self.the_map[y][x] = 2
	    temp = ''
	    prev = 0
            for i in range(len(self.route)):
                j = int(self.route[i])
                x += self.dx[j]
                y += self.dy[j]
                self.the_map[y][x] = 3
                self.route_coords.append( (y,x) )
		new_queue = ''

		#shity lambda eating around path (badly worked)
		if y+1 < self.m and self.mine.rows[y+1][x].show() == "\\":
			self.route_coords.append( (y+1,x) )
			self.extended_path += "1"
			self.search (y+1,x)
		if y-1 >= 0 and self.mine.rows[y-1][x].show() == "\\":
			self.route_coords.append( (y-1,x) )
			self.extended_path += "3"
			self.search (y-1,x)
		if x+1 < self.n and self.mine.rows[y][x+1].show() == "\\":
			self.route_coords.append( (y,x+1) )
			self.extended_path += "0"
			self.search (y,x+1)
		if x-1 >= 0 and self.mine.rows[y][x-1].show() == "\\":
			self.route_coords.append( (y,x-1) )
			self.extended_path += "2"
			self.search (y,x-1)
		new_queue += self.extended_path
		self.extended_path[::-1]
		for k in range(len(self.extended_path)):
			if self.extended_path[k] == "1":
				new_queue += "3"
			elif self.extended_path[k] == "3":
				new_queue += "1"
			elif self.extended_path[k] == "0":
				new_queue += "2"
			elif self.extended_path[k] == "2":
				new_queue += "0"
		self.extended_path = ''

		temp += self.route[prev:i]
		prev = i
		if new_queue:
			temp += str(new_queue)


	    self.route = temp

            self.the_map[y][x] = 4
#		print self.mine[y][x]
        return self.route_coords

    
    def prepare_map(self):
    
        row = [0] * self.n 
    
        for i in range(self.m): # create empty map
            self.the_map.append(list(row))

        c = 0
        r = 0
        for row in self.mine.rows:
            c = 0
            for col in row:                                                                                                                       
	        if col.show() == " " or col.show() == "." or col.show() == '\\':
                    self.the_map[r][c] = 0
        #		elif cell == "#":
        #			the_map[r][c] = 1
                elif col.show() == "R":
                    self.the_map[r][c] = 2
                    self.xA = c
                    self.yA = r
	        elif col.show() == "L":# or col.show() == '\\':
                    self.the_map[r][c] = 4
                    self.xB = c
                    self.yB = r
 	        elif r+2 < self.m and col.show() == "*":
		    self.the_map[r+2][c] = 1
                else:
                    self.the_map[r][c] = 1
                c = c + 1
            r = r + 1
	
    def print_map(self):
        # display the map with the route added
        print 'Map:'
        for y in range(self.m):
            for x in range(self.n):
                xy = self.the_map[y][x]
                if xy == 0:
                    print '.', # space
                elif xy == 1:
                    print 'O', # obstacle
                elif xy == 2:
                    print 'S', # start
                elif xy == 3:
                    print 'R', # route
                elif xy == 4:
                    print 'F', # finish
	    print
