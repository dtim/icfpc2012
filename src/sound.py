import sys
import pygame

class Sound:
    def __init__(self):
        freq = 44100
    	bitsize = -16
    	channels = 2
    	buffer = 2048
        pygame.mixer.init(freq, bitsize, channels, buffer)

    	self.music_file = "../music/popcorn.mid"
        pygame.mixer.music.load(self.music_file)
        pygame.mixer.music.play(-1, 0.0)

	self.mSound = pygame.mixer.Sound("../music/pop2.wav")
	self.dSound = pygame.mixer.Sound("../music/pop2.wav")
        
    def moveSound(self):
        self.mSound.play()

    def deathSound(self):
	self.dSound.play()

#    pygame.mixer.music.set_volume(0.75)


    """
    try:
      play_music(music_file)
    except KeyboardInterrupt:
      pygame.mixer.music.fadeout(1000)
      pygame.mixer.music.stop()
      raise SystemExit
    """

